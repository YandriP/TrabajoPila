/////////////////////////parte uno 
import java.io.*;
//Se importa la java.io.*para su uso en el programa
import javax.swing.JOptionPane;
//se importa javax para el uso de un menu al ejecutar el programa 

    public class Pila {
    //Se crea una clase el cual se nombre Pila y de esta forma da inicio a la clase
    public static final int MAX_LENGTH = 5;
    //Se crea un entero que poseera una tamaño maximo de 10 elemento el cual usaremos para determinar el tamaño de la pila 
    public static String Pila[] = new String[MAX_LENGTH];
    //Utilizamo un elemento de tipo String creado con el nombre de pila en cual estara conteniendo un tamaño maximo ya establecido
    public static int cima = -1;
    //Se crea un entero con el nombre cima que sera igual a -1 y este dara un resultado para la cima 
   
    public static String Pila2[] = new String[MAX_LENGTH];
    //Utilizamo un elemento de tipo String creado con el nombre de pila2 en cual estara conteniendo un tamaño maximo ya establecido
    public static int cima2 = -1;
    //Se crea un entero con el nombre cima2 que sera igual a -1 y este dara un resultado para la cima 
   
    public static String Pilaaux[] = new String[MAX_LENGTH];
    //Utilizamo un elemento de tipo String creado con el nombre de pilaaux en cual estara conteniendo un tamaño maximo ya establecido
    public static int cimaaux = -1;
    //Se crea un entero con el nombre cimaaux que sera igual a -1 y este dara un resultado para la cima 
   
    public static String Pilaaux2[] = new String[MAX_LENGTH];
    //Utilizamo un elemento de tipo String creado con el nombre de pilaaux2 en cual estara conteniendo un tamaño maximo ya establecido
    public static int cimaaux2 = -1;
    //Se crea un entero con el nombre cimaaux2 que sera igual a -1 y este dara un resultado para la cima 

    public static void main(String[] args)throws IOException{
        //se crea la variabble main el cual no debuelve ningun valor 
        Menu();
        //se crea la variable menu
    }
    //se cierra la variable main 
    
    public static void Menu()throws IOException{
        //se crea la variable menu el cual esta enlasada con la importacion que hicimos en el inicio
        String salida="====Menú Manejo Pila====\n"+"1- Insertar Elemento en pila 1\n"+ "2- Insertar Elemento en pila 2\n"+ "3- Eliminar ultimo elemento ingresado\n";
        //se utiliza un string con el nombre salida que permite enseñar por pantalla un mensaje 
        salida=salida+"4- Buscar elemento\n"+"5- Imprimir la pila1\n"+"6- Imprimir la pila2\n"+"7- Contar repetición\n" +"8-Comparar pilas \n" +"9- Salir\n";
        //volvemos a utilizar el string y continuamos a continuar con el mensaje que vamos a mostra
        String entra=JOptionPane.showInputDialog(null, salida);
        //se crea un elemento de tipo string con nombre entrada el cual utilizaremos el Optionpanel y el Input el cual ya importamos en 
        //el inico y nos permite enseñar por pantalla y mensaje y no permite tener una entrada por teclado
        int op = Integer.parseInt(entra);
        //cremaos una variable de tipo int con el nombre op el cual esta podre tener un enlase con el Integre para la entrada por teclado
        Opciones(op);
        //creamos la varialbe opciones el cual contiene a op
    }
    //cerramos la variable menu

    public static void Opciones(int op)throws IOException{
    //creamos la variable opciones de tipo estatico el cual esta enlasado con op 
    String salio;
    //se crea la variable salio de tipo string 
    switch(op){
        //utilizamos la sentencia switch el cual nos permite tener un numero de opciones 
            case 1: Insertar();
            //creamos la sentencia inserta 
            Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente 
            break;
            //cerramos la sentencia 
                                        
            case 2: Insertar2();
            //creamos la sentencia insertar2
            Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente 
            break;
            //cerramos la sentencia 
                                
            case 3: salio=Desapilar();
            //creamos la sentencia salio el cual sera igual a la variable desapilar
            if (!vacia()){
                //utilizamos la sentencua if el cual nos indica que vacia sera diferente en este caso
            JOptionPane.showMessageDialog(null, "El dato que salio es "+salio); 
            //mostrara por panel de opciones un dialogo 
            }
            //cerramos la sentencia if 
            Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente 
            break;
            //cerramos la sentencia 
	
            case 4: Buscar();
            //creamos la sentencia buscar
            Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente 
            break;
            //cerramos la sentencia 
	
            case 5: Imprimir();
            //creamos la sentencia imprimir 
            Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente 
            break; 
            //cerramos la sentencia 
                                
            case 6: Imprimir2();
            //creamos la sentencia imprimir2
            Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente 
            break;
            //cerramos la sentencia 
            
            case 7: contar();    
            //creamos la sentencia contar
            Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente 
            break;
            //cerramos la sentencia 
			
            case 8: comparar();  
            //creamos la sentencia compara 
            default:Menu();
            //utilizamos la variable opciones el cual fue creada anteriormente y el cual mostrara de forma default 
            //si este no muestra nada el la operacion 
            break;
            //cerramos la sentencia 
                                
            case 9: System.exit(0);
            //salimos de las operaciones de las sentencias 
            break;  
            //cerramos la sentencia 
            }
            //cerramos la sentencia switch 
        }
    //cerramos la variable opciones 

        public static void Insertar()throws IOException{
        //Creamos la variable insertar 
        String entra = JOptionPane.showInputDialog("Digite un Elemento para la pila");
        //cremos un elemento de tipo string llamado entrada el cual  mostra en el panel de opciones un mensaje 
        Apilar(entra);
        //cremaos la varialbe apilar el cual estara enlasada con entrada 
        }
        //cerramos la varialbe insertar 
    
        public static void Insertar2()throws IOException{
        //creamos la varialbe insetar2
        String entra = JOptionPane.showInputDialog("Digite un Elemento para la pila2");
        //cremos un elemento de tipo string llamado entrada el cual  mostra en el panel de opciones un mensaje 
        Apilar2(entra);
        //cremaos la varialbe apilar el cual estara2 enlasada con entrada 
        }
        //cerramos la variable insertar2

        
        public static void Apilar(String dato)throws IOException{
        //creamos la varialbe apilar de tipo estatica el cual esta enlasado con dato
        if ((Pila.length-1)==cima){
            //creamos la sentencia de tipo if el cual indica si pila es solo igual a cima 
            JOptionPane.showMessageDialog(null,"Capacidad de la pila al limite");
            //muestra por el panel de opciones si es correcto 
            Imprimir();
            //utilizamos la varialbe imprimir el cual creamos anteriormente 
        }else{
            //cerramos el if y abrimos el else en caso de que no sea correcto la sentencia
            cima++;
            //si la sentencia es correcta la aumentara en uno la cima 
            JOptionPane.showMessageDialog(null,"Cima en la posición "+cima);
            //muestra por el panel de opciones si es correcto 
            Pila[cima]=dato;
            //la variable pila en cual esta enlasada con la cima sera igual a dato
            }
            //cerramos la sentencia else 
        }
        //cerramos la varialbe apilar 
    
        public static void Apilar2(String dato)throws IOException{
        //creamos la varialbe apilar2 de tipo estatica el cual esta enlasado con dato
        if ((Pila.length-1)==cima2){
            //creamos la sentencia de tipo if el cual indica si pila es solo igual a cima2
            JOptionPane.showMessageDialog(null,"Capacidad de la pila al limite");
            //muestra por el panel de opciones si es correcto 
            Imprimir();
            //utilizamos la varialbe imprimir el cual creamos anteriormente 
        }else{
            //cerramos el if y abrimos el else en caso de que no sea correcto la sentencia
            cima2++;
            //si la sentencia es correcta la aumentara en uno la cima2
            JOptionPane.showMessageDialog(null,"Cima en la posición "+cima2);
            //muestra por el panel de opciones si es correcto 
            Pila2[cima2]=dato;
            //la variable pila2 en cual esta enlasada con la cima2 sera igual a dato
            }
            //cerramos la sentencia else 
        }
         //cerramos la varialbe apilar2 
   
        public static void Apilaraux(String dato)throws IOException{
        //creamos la varialbe apilaraux de tipo estatica el cual esta enlasado con dato
        if ((Pilaaux.length-1)==cimaaux){
            //creamos la sentencia de tipo if el cual indica si pila es solo igual a cimaaux 
            JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar al limite");
            //muestra por el panel de opciones si es correcto 
        }else{
            //cerramos el if y abrimos el else en caso de que no sea correcto la sentencia
            cimaaux++;
            //si la sentencia es correcta la aumentara en uno la cimaaux
            Pilaaux[cimaaux]=dato;
            //la variable pilaaux en cual esta enlasada con la cimaaux sera igual a dato
            }
            //cerramos la sentencia else 
        }
         //cerramos la varialbe apilaraux 
    
        public static void Apilaraux2(String dato2)throws IOException{
        //creamos la varialbe apilaraux2 de tipo estatica el cual esta enlasado con dato
        if ((Pilaaux2.length-1)==cimaaux2){
            //creamos la sentencia de tipo if el cual indica si pila es solo igual a cimaaux2 
            JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar al limite");
            //muestra por el panel de opciones si es correcto 
        }else{
            //cerramos el if y abrimos el else en caso de que no sea correcto la sentencia
            cimaaux2++;
            //si la sentencia es correcta la aumentara en uno la cimaaux
            Pilaaux2[cimaaux2]=dato2;
            //la variable pilaaux2 en cual esta enlasada con la cimaaux2 sera igual a dato
            }
            //cerramos la sentencia else 
        }
         //cerramos la varialbe apilaraux2 

        public static boolean vaciaaux(){
            //creamos la variable de tipo boolean llamada vaciaaux
        return (cimaaux==-1);
        //usamos un return indicando que la cimaaux 
        }
        //cerramos la varialbe vaicaaux

        public static boolean vacia(){
            //creamos la variable de tipo boolean llamada vacia
        return (cima==-1);
        //usamos un return indicando que la cima
        }
        //usamos un return indicando que la cima
        
        public static boolean vacia2(){
            //creamos la variable de tipo boolean llamada vacia2
        return (cima==-1);
        //usamos un return indicando que la cimaa
        }
        //usamos un return indicando que la cima2
////////////////////////////////////////Parte 2 
        public static void Imprimir()throws IOException{
            //creamos una nueva extencion publica utilizando el elemento imprimir()el cual contendra otra sirie de elemento
            String quedata,salida=" ";
        if (cima!=-1)
            //trae una condicion que indica que la cima cera igual a -1 esto en el programa  dice si la pila esta vacia
        {do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
            quedata=Desapilar();
            //los elementos desapilados de la pila se van a guardar en quedato
            salida=salida+quedata+"\n";
            Apilaraux(quedata);            
            //los elementos desapilados de la pila auxiliar se van a guardar en quedata
        }while(cima!=-1);
 //creamos una estructura wile con los nombres cima que va a poseer un valor de -1 representara en el programa la cima de la pila 
                do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
            quedata=Desapilaraux();
            //los elementos desapilados de la pila auxiliar se van a guardar en quedato
            Apilar(quedata);
        }while(cimaaux!=-1);
 //creamos unSa estructura wile con los nombres cimaaux que va a poseer un valor de -1 representara en el programa la cima de la pila 
            JOptionPane.showMessageDialog(null, salida);
        }
       //cierra la clase
        else{
          //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones
          JOptionPane.showMessageDialog(null, "La pila esta vacía");
            }
      //cierra la clase
        }
      //cierra la clase
           public static void Imprimir2()throws IOException{
      //creamos una nueva extencion publica utilizando el elemento imprimir2()el cual contendra otra sirie de elemento
        String quedata2,salida=" ";
        if (cima2!=-1)
     //trae una condicion que indica que la cima2 cera igual a -1 esto en el programa en el programa dise si la pila esta vacia   
        {do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
            quedata2=Desapilar2();
            //los elementos desapilados de la pila se van a guardar en quedato
            salida=salida+quedata2+"\n";
            Apilaraux(quedata2);
//los elementos apilados de la pila auxiliar se van a guardar en quedato2            
        }while(cima2!=-1);
        //creamos una estructura wile con los nombres cima2 que va a poseer un valor de -1 representara en el programa la cima de la pila 
        
        do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
            quedata2=Desapilaraux();
            //los elementos desapilados de la pila auxiliar se van a guardar en quedata2 
            Apilar(quedata2);
        }while(cimaaux2!=-1);
     //utiliza una estructura repetitiva wile y elimina elementos de la cima de la pila auxiliar
            JOptionPane.showMessageDialog(null, salida);
        }
         //cierra la clase
        else {
          //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones
            JOptionPane.showMessageDialog(null, "La pila esta vacía");
            }
        //cierra la clase
        }
        //cierra la clase

    
        public static String Desapilar()throws IOException{
  //creamos una nueva extencion publica utilizando el elemento desapilar()el cual contendra elementos de tipo string
            String quedato;
        if(vacia()){
  //trae una condicion que indica que la cima esta vacia, esto en el programa en el programa dise si la pila esta vacia   
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía!" );
            return("");
            //retorna un valor
        }else{
            //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones
            quedato=Pila[cima];
            Pila[cima] = null;
            //indica que el elemento pila que contiene la cima sera igual a null que quiere decir que no tendra nada
            --cima;
         //elimina elementos de la cima de la pila 
            return(quedato);
            }
        //cierra la clase
                }
        //cierra la clase

        public static String Desapilar2()throws IOException{
    //creamos una nueva extencion publica utilizando el elemento desapilar2()el cual contendra elementos de tipo string
            String quedato2;
        if(cima2==-1){
   //trae una condicion que indica que la cima2 cera igual a -1 esto en el programa en el programa dise si la pila esta vacia   
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía!" );
            return("");
            //retorna un valor
        }else{
       //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones
            quedato2=Pila2[cima2];
            Pila2[cima2] = null;
            --cima2;
      //elimina elementos de la cima de la pila 

            return(quedato2);
            }
       //cierra la clase
        }
      //cierra la clase
    
        public static String Desapilaraux()throws IOException{
  //creamos una nueva extencion publica utilizando el elemento desapilaraux()el cual contendra elementos de tipo string
            String quedato;
        if(cimaaux== -1){
     //trae una condicion que indica que la cimaaux cera igual a -1 esto en el programa en el programa dise si la pila esta vacia   
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía!" );
            return("");
            //retorna un valor
        }else{
               //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones
             quedato=Pilaaux[cimaaux];
             //los elementos de la cima de la pila auxiliar se van a guardar en quedato
	      Pilaaux[cimaaux] = null;
              //la cima de la pila auxiliar esta vacia
	      --cimaaux;
           //elimina elementos de la cima de la pila auxiliar
              return(quedato);
              //retorna un valor de tipo quedato
            }
       //cierra la clase
        }
      //cierra la clase
        
        public static String Desapilaraux2()throws IOException{
   //creamos una nueva extencion publica utilizando el elemento desapilaraux2()el cual contendra elementos de tipo string
            String quedato2;
        if(cimaaux2== -1){
     //trae una condicion que indica que la cimaaux2 cera igual a -1 esto en el programa en el programa dise si la pila esta vacia   
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía!" );
            return("");
            //retorna un valor
        }else{
             //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones

            quedato2=Pilaaux2[cimaaux2];
            //el valor de la cima de la pila  auxiliar se va a guardar en quedato
	    Pilaaux2[cimaaux2] = null;
	    --cimaaux2;
            //elimina elementos de la cima de la pila auxiliar
            return(quedato2);
            //retorna un valor de tipo quedato2
           }
      //cierra la clase
        }
     //cierra la clase
        public static void Buscar()throws IOException{
 //creamos una nueva extencion publica utilizando el elemento buscar()el cual contendra otra sirie de elemento
            if (vacia()){
     //trae una condicion que indica que la cima esta vacia, esto en el programa en el programa dise si la pila esta vacia   
                JOptionPane.showMessageDialog(null, "La pila esta vacìa");
            }
      //cierra la clase
            else{
               //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones

                String cad = JOptionPane.showInputDialog("Digite la cadena a buscar: ");
                String quedata;
                int bandera=0;
            do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
                quedata=Desapilar();
                //quedata va a desapilar elementos de la pila
                if(cad.equals(quedata)){
                bandera=1; //si esta
                }
            //cierra la clase
                Apilaraux(quedata); 
                //quedata va a apilar elementos de la pila auxiliar
            }while(cima!=-1);
//creamos una estructura wile con los nombres cima que va a poseer un valor de -1 representara en el programa la cima de la pila 
            do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
                quedata=Desapilaraux();
                //el valor de tipo quedato va a eliminar elementos de la pila auxiliar
                Apilar(quedata);
                //quedato va apilar elementos a la plla
            }while(cimaaux!=-1);
//creamos una estructura wile con los nombres cimaaux que va a poseer un valor de -1 representara en el programa la cima de la pila 
            if (bandera==1) {
                JOptionPane.showMessageDialog(null,"Elemento encontrado");
            }else{
             //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones

                JOptionPane.showMessageDialog(null,"Elemento no encontrado");
                }
         //cierra la clase
            }
        //cierra la clase
        }
        //cierra la clase
    ////////////////////////////////////////////////////////
        public static void comparar()throws IOException{
            //crea una extencion con el cual usa los nombres de los casos se swit indicando el uso de que este contiene
            if (vacia()){
      //con el if creamos una condicion dentro del parentesis en el cual  la pila indica que esta vacia
            JOptionPane.showMessageDialog(null, "La pila esta vacìa");
            }
       //cierra la clase
            else{
      //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones
                String quedata;
                String quedata2; 
                int bandera=0; //no se encuentra
            do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
                quedata=Desapilar();
                //quedata va a despilar elementos de la pila
                quedata2=Desapilar2();   
                //quedata va a desapilar elementos de la pila2
            if(quedata2.equals(quedata)){
                bandera=1; //si esta
            }
        //cierra la clase
                Apilaraux(quedata);
                //quedata va a apilar elementos a la pila auxiliar
                Apilaraux2(quedata2);    
                //quedata va a apilar elementos a la pila auxiliar
            }while(cima!=-1);
 //creamos una estructura wile con los nombres cima que va a poseer un valor de -1 representara en el programa la cima de la pila 
            do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
                quedata=Desapilaraux();
                //quedata va a desapilar elementos a la pila auxiliar
                Apilar(quedata);    
                //quedata va a apilar elementos a la pila  
                quedata2=Desapilaraux2();
                //quedata va a desapilar elementos de la pila aux2
                Apilar2(quedata2);     
                //quedata va a apilar elementos a la pila2
            }while(cimaaux!=-1);
//creamos una estructura wile con los nombres cimaaux que va a poseer un valor de -1 representara en el programa la cima de la pila 
                if (bandera==1) {
                JOptionPane.showMessageDialog(null,"Elemento -" + quedata +"-y-"+quedata2+"---son iguales" );
            }else{
     //es el caso de que la condicion no sea correcta abrira un else en el cual adentro de el encontraremos otras condiciones
                JOptionPane.showMessageDialog(null,"Pila1 y Pila2 no son iguales");
                }
          //cierra la clase
            }
         //cierra la clase
        }
       //cierra la clase
    //////////////////////////////////////////////
        public static void contar() throws IOException {
  //creamos una nueva extencion publica utilizando el elemento contar()el cual contendra otra sirie de elemento
            String quedata;
            int contador = 0;
            //el contador va a ser igual a un valor entero
        if (cima!=-1)
         //trae una condicion que indica que la sima cera igual a -1 esto en el programa en el programa dise si la pila esta vacia   
            {do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
            quedata=Desapilar();
            //quedata va a desapilar elementos de la pila
            contador = contador+1;
            //contador que va aumentando sus valores de uno en uno
            Apilaraux(quedata);           
            //quedata va a apilar elementos de la pila auxilar
            }while(cima!=-1);
//creamos una estructura wile con los nombres cima que va a poseer un valor de -1 representara en el programa la cima de la pila 
            do{
//estructura de control de la mayoría de los lenguajes de programación estructurados cuyo propósito es ejecutar un bloque de código
            quedata=Desapilaraux();
            //quedata va a desapilar elementos de la pila auxiliar 
            Apilar(quedata);
            //quedata va a apilar elementos de la pila
            }while(cimaaux!=-1);
//creamos una estructura wile con los nombres cimaaux que va a poseer un valor de -1 representara en el programa la cima de la pila 
                JOptionPane.showMessageDialog(null,"Elementos en la pila: "+ contador);
            }
        //cierra la clase
            else{
                JOptionPane.showMessageDialog(null, "La pila esta vacìa");
            }
        //cierra la clase
        }
      //cierra la clase
        }
     //cierra la clase
